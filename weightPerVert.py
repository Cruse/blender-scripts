# Parameters
groupFrom = "RHS"
groupTo = "upper"
manualSelect = True

import bpy
import bmesh

import math

# Get the active mesh
obj = bpy.context.active_object

bpy.ops.object.mode_set(mode="EDIT")

obj = bpy.context.edit_object

me = obj.data

groupFromIndex = obj.vertex_groups[groupFrom].index
groupToIndex = obj.vertex_groups[groupTo].index

if(manualSelect):
    bpy.ops.mesh.select_all(action='SELECT')

verts = [[v.index, [g.group for g in v.groups]] for v in me.vertices if v.select] 

bpy.ops.object.mode_set(mode="OBJECT")

print("gotverts")

#defaults 1
wDef = 0

#custom 1
wDef = .08

#defaults 2
wStart = 1
wEnd = wDef
first = 0
last = math.ceil(len(verts) / 2)

#custom 2
first = 0
last = 113

#defaults 3
changeAt = first
changeEnd = last

#custom 3
changeAt = 67
changeEnd = last

#defaults 4
wDelta = wEnd - wStart
steps = changeEnd - changeAt + 1
linStep = wDelta / steps

import scipy.interpolate as interp
import numpy as np

#default 1
slowstart = True

#vertex form values
va = -2
vh = 0
vk = abs(wDelta)

#calculated standard form values
sa = va
sb = va * vh * 2
sc = va * (vh **2) + vk

# get x intercept
xint = 0
if((vk > 0 and va < 0) or (vk < 0 and va > 0)):
    xint = vh + abs((-sb + -math.sqrt(2 * sb**2  - 4 * sa * sc)) / (2 * sa))
else:
    print("error")


xstart = 0
xend = xint
if(slowstart):
    xstart = 0
    xend = xint
else:
    xstart = xint
    xend = 0

range_x = np.linspace(xstart, xend, num=steps,endpoint=True)

range_y = [max((va * (i - vh) **2 + vk), 0) for i in range_x]

func = interp.interp1d(range_x,range_y, 'quadratic')

#get max and min z values
changeZs = [v.co[2] for v in me.vertices if v.index >=changeAt and v.index <= changeEnd]

minZ = min(changeZs)
maxZ = max(changeZs)
#for i in range(changeAt, changeEnd + 1):
#    z = me.vertices[i].co[2]
#    if(z > maxZ):
#        maxZ = z
#    if(z < minZ):
#        minZ = z

print(minZ)
print(maxZ)
print()

zDelta = maxZ - minZ
zMultiplier = 1 / zDelta
zMultiplier *= xint

mirror = True

weights = list()
for i,vert in enumerate(verts):
    inFrom = False
    inTo = False
    for g in vert[1]:
        if(g == groupFromIndex):
            inFrom = True
        #if(g == groupToIndex):
        #    inTo = True
    if(not inFrom):# or not inTo):
        continue
    v = vert[0]
    weight = wDef
    if(v < first or v > last):
        obj.vertex_groups[groupTo].add([v], weight, "REPLACE")
    else:
        if(v < changeAt or v > changeEnd):
            weight = wStart
            obj.vertex_groups[groupTo].add([v], weight, "REPLACE")
        else:
            index = v - changeAt
            x = (changeZs[index] - minZ) * zMultiplier
            thisStep = func(min(xint,x))
            if(slowstart):
                weight = wStart - thisStep
            else:
                weight = wEnd + thisStep
            weight = max(wEnd,min(weight,wStart))
            obj.vertex_groups[groupTo].add([v], weight, "REPLACE")
    weights.append(weight)

if(mirror):
    newStart = len(verts) - 1
    newEnd = math.floor(len(verts) / 2)
    for i,vert in enumerate(verts):
        v = vert[0]
        if(v <= newStart and v >= newEnd):
            index = newStart - v
            print(index)
            weight = weights[index]
            obj.vertex_groups[groupTo].add([v], weight, "REPLACE")
    

# Get a BMesh representation
#bm = bmesh.from_edit_mesh(me)

#bm.faces.active = None

#last = -1
#next = 0
#vertices = list(list())
#vertices.append(next)
#for i in range(0, len(bm.edges) - 1):
#    found = False
#    count = 0
#    while (count < len(bm.edges) and not found):
#        v1 = bm.edges[count].verts[0].index
#        v2 = bm.edges[count].verts[1].index
#        if(v1 == next and v2 != last):
#            last = next
#            next = v2
#            found = True
#        elif(v2 == next and v1 != last):
#            last = next
#            next = v1
#            found = True
#        count += 1
#    vertices.append(next)