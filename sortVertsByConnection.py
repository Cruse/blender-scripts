# Parameters
sortFirst = False

import bpy
import bmesh

# Get the active mesh
obj = bpy.context.edit_object
me = obj.data


# Get a BMesh representation
bm = bmesh.from_edit_mesh(me)

bm.faces.active = None

# Modify the BMesh, can do anything here...
#for v in bm.verts:
#    print(tuple(v.co) )

# optional sort by selection
if(sortFirst):
    bpy.ops.mesh.sort_elements(type="SELECTED")

last = -1
next = 0
vertices = list(list())
vertices.append(next)
for i in range(0, len(bm.edges) - 1):
    found = False
    count = 0
    while (count < len(bm.edges) and not found):
        v1 = bm.edges[count].verts[0].index
        v2 = bm.edges[count].verts[1].index
        if(v1 == next and v2 != last):
            last = next
            next = v2
            found = True
        elif(v2 == next and v1 != last):
            last = next
            next = v1
            found = True
        count += 1
    vertices.append(next)

realIndices = list()
for i in vertices:
    for index,vert in enumerate(bm.verts):
        if(vert.index == i):
            realIndices.append(index)

for index,i in enumerate(realIndices):
    bm.verts[i].index = index
    print(vertices[index])

bm.verts.sort()