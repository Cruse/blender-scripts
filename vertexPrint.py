#parameters
applyMods = True
overwriteExisting = True
frameStart = 0
frameEnd = 60
frameStep = 1
vertsPerLine = 64
toVector3 = False
padVertChars = 33
if(toVector3):
    padVertChars = 41 
compactMode = True
if(compactMode):
    vertsPerLine = 1000
delimited = False
limiter = 4 # simple exclusion of 1-1/n * vertices
minDist = 0.005
constant = True
fromAnywhere = True
objectName = 'mouth'

pdef = '#define mdata'

dumbFrameOffset = 1

import bpy
import bmesh

import sys
import os

import math
from printOverride import *

# setup keys
bpy.context.scene.frame_set(dumbFrameOffset + frameStart)

if(fromAnywhere):
	bpy.ops.object.mode_set(mode="OBJECT")
	bpy.context.view_layer.objects.active = bpy.data.objects[objectName]

me = bpy.context.active_object.data
vertCount = len(me.vertices)

frameRange = frameEnd - frameStart
frameSteps = math.ceil(frameRange / frameStep)
fps = 50#bpy.context.scene.render.fps

anim = me.name.replace(" ", "-")
n = bpy.context.scene.frame_current
#filestring = f'C:/Users/assets/vData/vertexData_{anim:}_{frameStart:03}_{frameEnd:03}_{frameStep:02}.cpp'
filestring = 'C:/Users/proj/T3D/T3D/mData.h'

import os.path
if(not overwriteExisting and os.path.isfile(filestring)):
    console_print("FILE ALREADY EXISTS")
    raise ValueError("File already exists")
    sys.exit()
f = open(filestring, 'w')
nl = "\n"
tab = " " * 4
dtab = "" + tab * 0

pStr = ""
if(constant):
    pStr = "#define"

endstr = nl
if(constant):
    endstr = ' '
endstr = f'{nl}'


f.write(\
f"""#pragma once
#include "Vector3.h"
#include <vector>

// Initialise data
// Values generated via python script on blender scene
// - frames {frameStart} to {frameEnd} at a default interval of {frameStep} seconds""" + nl\
)

pStr = "#define mdata"

f.write(\
dtab + f'{pStr}_originalVerts {vertCount}' + endstr + \
dtab + f'{pStr}_frameStart {frameStart}' + endstr + \
dtab + f'{pStr}_frameEnd {frameEnd}' + endstr + \
dtab + f'{pStr}_frameStep {frameStep}' + endstr + \
dtab + f'{pStr}_fps {fps}' + endstr \
)

frames = list(range(frameStart,frameEnd + 1 + frameEnd % frameStep, frameStep))
f.write(dtab + f'{pStr}_frameSteps {len(frames)}' + endstr)

f.write(nl * 1)
for fcount,fi in enumerate(frames):    
    #next keyframe
    bpy.context.scene.frame_set(dumbFrameOffset + fi)
    
    #get mesh
    # Get the active mesh
    obj = bpy.context.active_object
    me = obj.data

    meshVerts = me.vertices

    if(applyMods):
        depsgraph = bpy.context.evaluated_depsgraph_get()
        bm = bmesh.new()
        bm.from_object(obj, depsgraph)
        bm.verts.ensure_lookup_table()
        meshVerts = bm.verts

    verts = ""
    for v in meshVerts:
        verts += str(tuple(v.co)) + "\n"

    if(applyMods):
        bm.free()
        
    verts = verts.strip()
    verts = verts.replace("f", "")
    verts = verts.replace(")", "")
    verts = verts.replace("(", "")
    verts = verts.replace(";", "")
    verts = verts.replace(" ", "")
    verts = verts.splitlines()

    scale = 1.0
    p = 6

    verts3raw = list(list())
    for realvindex,i in enumerate(verts):
        if((delimited) or (realvindex % limiter == 0)):
            valid = True

            strvert = i.split(",")
            
            vert = [0,0,0]
            vert[0] = float(strvert[0]) * scale
            vert[1] = float(strvert[2]) * scale
            vert[2] = float(strvert[1]) * scale    

            verts3raw.append(vert)

    verts3sorted = sorted(verts3raw, key=lambda tup: tup[0])

    verts3Remove = list(list())
    lastX = verts3sorted[-1][0]
    lastY = verts3sorted[-1][1]
    for vindex,vert in enumerate(verts3sorted):
        tempX = vert[0]#round(vert[0], p)
        tempY = vert[1]#round(vert[1], p)
        difference = abs(tempX - lastX) + abs(tempY - lastY)
        if(difference < minDist):# and (tempX != lastX or tempY != lastY)):
            verts3Remove.append(vert)
        lastX = tempX
        lastY = tempY

    for vindex in range(0, len(verts3Remove)):
        verts3raw.remove(verts3Remove[vindex])

    verts3 = verts3raw
    
    minX = 0.0
    maxX = 0.0
    minY = 0.0
    maxY = 0.0
    for vindex,vert in enumerate(verts3):
        if(vert[0] < minX):
            minX = vindex
        if(vert[0] > maxX):
            maxX = vindex
        if(vert[1] < minY):
            minY = vindex
        if(vert[1] > maxY):
            maxY = vindex

    strVerts3 = list(list())
    for i,vert in enumerate(verts3):
        strVert = list()
        for j,co in enumerate(vert):
            co = round(co, p)
            strco = str(co)
            if(co == 0.0):
                strco = '0.0'
            strVert.append(strco + "f")
        strVerts3.append(strVert)

    vertCount = len(strVerts3)
    
    tabs = 0
    ttab = dtab + tab * tabs
    console_print(f'writing frame {fi:03}, vc = {vertCount:03}')
    print(f'writing frame {fi:03}')
    f.write(\
    ttab + f'// vData for frame {fi:03}' + nl \
    )
    pStr = pdef + f'_verts_{fcount}'
    f.write( \
    ttab + f'{pStr} ' +\
    ttab + f'{{')# + nl)
    
    tabs = 0
    ttab = dtab + tab * tabs
    f.write(ttab)
    encl = ["{","}"]
    if(toVector3):
        encl = ["Vector3(", ")"]
    for i in range(0, vertCount - 1):
        vert = f'{", ".join(strVerts3[i])}'
        tpad = padVertChars + len(tab)
        endstr = ", "  
        if((i + 1) % vertsPerLine == 0):
            tpad = 0
            endstr = ',' + nl + ttab
        f.write(f'{vert.join(encl) + endstr: <{tpad}}')
                
    if((vertCount - 1) % vertsPerLine == 0):
        f.write(nl)
    vert = f'{", ".join(strVerts3[vertCount - 1])}'
    f.write(f'{vert.join(encl)}')# + nl)
    
    tabs = 0
    ttab = dtab + tab * tabs
    f.write(ttab + "}")
    
    pStr = "#define mdata"

    if(compactMode):
        f.write(nl)
        endstr = ' '
        if(constant):
            endstr = f'{nl}'
        f.write(ttab + f"{pStr}_vertCount_{fcount} {vertCount}" + endstr)
    
        f.write(ttab + f"{pStr}_minX_{fcount} {minX}" + endstr)
        f.write(ttab + f"{pStr}_maxX_{fcount} {maxX}" + endstr)

        f.write(ttab + f"{pStr}_minY_{fcount} {minY}" + endstr)
        f.write(ttab + f"{pStr}_maxY_{fcount} {maxY}" + endstr)
        
        endstr = f'{nl}'
        f.write(ttab + f"{pStr}_frame_{fcount} {fi}" + endstr)

        if(fcount + 1 < len(frames)):
            f.write(nl)
    else:
        endstr = f';{nl}'
        f.write(nl * 2)
        f.write(ttab + f"{pStr}_vertCount_{fcount} {vertCount}" + endstr)
        
        f.write(ttab + f"{pStr}_minX_{fcount} {minX}" + endstr)
        f.write(ttab + f"{pStr}_maxX_{fcount} {maxX}" + endstr)

        f.write(ttab + f"{pStr}_minY_{fcount} {minY}" + endstr)
        f.write(ttab + f"{pStr}_maxY_{fcount} {maxY}" + endstr)
        
        f.write(ttab + f"{pStr}_frame_{fcount} {fi}" + endstr)

        if(fcount + 1 < len(frames)):
            f.write(nl * 2)
            
f.write(nl * 1)

f.write("namespace T3D {" + nl)

dtab = "" + tab * 1

tabs = 0
ttab = dtab + tab * tabs
f.write(ttab + "struct MouthData {" + nl)

endstr = nl

tabs = 2
ttab = dtab + tab * tabs
f.write(\
ttab + f'MouthData () :' + endstr + \
#dtab + "{" + endstr +\
#dtab + f'    { str({vertCount}) + ", ": <7}// original vert ount' + endstr + \
#dtab + f'    { str({frameStart}) + ", ": <7}// frameStart' + endstr + \
#dtab + f'    { str({frameEnd}) + ", ": <7}// frameEnd' + endstr + \
#dtab + f'    { str({frameStep}) + ", ": <7}// frameStep' + endstr + \
#dtab + f'    { str({fps}) + " ": <7}// fps' + endstr + \
#dtab + f'}});' + nl)
ttab + f'{ "origVertCount(mdata_originalVerts)" + ", ": <7}// original vert count' + endstr + \
ttab + f'{ "frameStart(mdata_frameStart)" + ", ": <7}// frameStart' + endstr + \
ttab + f'{ "frameEnd(mdata_frameEnd)" + ", ": <7}// frameEnd' + endstr + \
ttab + f'{ "frameStep(mdata_frameStep)" + ", ": <7}// frameStep' + endstr + \
ttab + f'{ "frameRange(frameEnd- frameStart)" + ", ": <7}// frames in animation' + endstr + \
ttab + f'{ "frameSteps(mdata_frameSteps)" + ", ": <7}// frameSteps' + endstr + \
ttab + f'{ "fps(mdata_fps)" + ", ": <7}// fps' + endstr + \
ttab + "frame(new int[frameSteps])," + endstr + \
ttab + "vertCount(new int[frameSteps])," + endstr + \
#ttab + "verts(new std::vector<Vector3>[frameSteps])," + endstr + \
ttab + "verts(new Vector3*[frameSteps])," + endstr + \
ttab + "minX(new int[frameSteps])," + endstr + \
ttab + "maxX(new int[frameSteps])," + endstr + \
ttab + "minY(new int[frameSteps])," + endstr + \
ttab + "maxY(new int[frameSteps])" + endstr)


f.write(ttab + "{" + nl)
ttab = dtab + tab * 2
for fcount,fi in enumerate(frames):
    pStr = f'mdata_verts_{fcount}'
    f.write( \
    #ttab + f'verts[{fcount}].reserve(mdata_vertCount_{fcount});' + nl +\
    ttab + f'verts[{fcount}] = new Vector3[mdata_vertCount_{fcount}] {pStr};' + nl\
    )

    if(compactMode):
        endstr = " "
        f.write(ttab + f"{f'vertCount[{fcount}] = mdata_vertCount_{fcount};{endstr}': <7}")
    
        f.write(ttab + f"{f'minX[{fcount}] = mdata_minX_{fcount};{endstr}': <7}")
        f.write(ttab + f"{f'maxX[{fcount}] = mdata_maxX_{fcount};{endstr}': <7}")

        f.write(ttab + f"{f'minY[{fcount}] = mdata_minY_{fcount};{endstr}': <7}")
        f.write(ttab + f"{f'maxY[{fcount}] = mdata_maxY_{fcount};{endstr}': <7}")
        
        f.write(ttab + f"{f'frame[{fcount}] = mdata_frame_{fcount};{endstr}': <7}")

    else:
        f.write(nl)
        endstr = nl
        f.write(ttab + f"vertCount[{fcount}] = mdata_vertCount_{fcount};" + endstr)
    
        f.write(ttab + f"minX[{fcount}] = mdata_minX_{fcount};" + endstr)
        f.write(ttab + f"maxX[{fcount}] = mdata_maxX_{fcount};" + endstr)

        f.write(ttab + f"minY[{fcount}] = mdata_minY_{fcount};" + endstr)
        f.write(ttab + f"maxY[{fcount}] = mdata_maxY_{fcount};" + endstr)
        
        f.write(ttab + f"frame[{fcount}] = mdata_frame_{fcount};")

    f.write(nl)
    if(fcount + 1 < len(frames)):
        f.write(nl)
        
tabs = 1
ttab = dtab + tab * tabs
f.write(ttab + "};" + nl * 2);
f.write( \
"""		// general values
		const int origVertCount;
		const int frameStart;
		const int frameEnd;
		const int frameStep;

		const int frameRange;
		const int frameSteps;
		//static const int stcFrameSteps;

		const float fps;

		// values per vert
		int* frame;
		int* vertCount;
		Vector3 ** verts;

		// indexes for useful vertices
		int* minX;
		int* maxX;
		int* minY;
		int* maxY;
	};
}"""\
)

f.close()
print("done")
console_print("done")